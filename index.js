const Random = require('./lib/randomString')
const HMAC = require('./lib/hmac')
const BASE64URL = require('base64url')
const _ = require('lodash')

module.exports = class JsonWebToken {
  constructor () {
    this.header = {alg: 'HS256', typ: 'JWT'}
    this._claims = {iss: '', sub: '', aud: '', exp: '', nbf: '', iat: '', jti: ''}
    this._payload = {}
    this.signature = ''
    this.secret = Random()
    this._encoded = ''
  }
  get payload () {
    return _.assign(this._payload, this.claims)
  }
  get claims () {
    let claims = {}
    for (let claim in this._claims) {
      if (this.claim(claim)) claims[claim] = this._claims[claim]
    }
    return claims
  }
  get alg () {
    return this.header.alg
  }
  get iss () {
    return this.claims('iss')
  }
  get sub () {
    return this.claims('sub')
  }
  get aud () {
    return this.claims('aud')
  }
  get exp () {
    return this.claims('exp')
  }
  get nbf () {
    return this.claims('nbf')
  }
  get iat () {
    return this.claims('iat')
  }
  get jti () {
    return this.claims('jti')
  }
  set payload (json) {
    this._payload = json
  }
  set alg (value) {
    this.header.alg = value
  }
  set iss (value) {
    this._claims.iss = value
  }
  set sub (value) {
    this._claims.sub = value
  }
  set aud (value) {
    this._claims.aud = value
  }
  set exp (value) {
    this._claims.exp = value
  }
  set nbf (value) {
    this._claims.nbf = value
  }
  set iat (value) {
    this._claims.iat = value
  }
  set jti (value) {
    this._claims.jti = value
  }
  claim (claim) {
    if (this._claims[claim] !== '') return this._claims[claim]
    else return false
  }
  identifyAlg (alg) {
    let a = alg.toUpperCase().split(/(s)/i)
    let algoritm = {}
    switch (a[0]) {
      case 'H':
        algoritm.use = 'HMAC'
        break
      case 'R':
        algoritm.use = 'RSASSA'
        break
      case 'E':
        algoritm.use = 'ECDSA'
        break
      default:
        algoritm.use = 'NULL'
    }
    if (a[2]) {
      algoritm.base = 'sha' + a[2]
    }
    return algoritm
  }
  newSecret (size = 16) {
    this.secret = Random(size)
  }
  newId (size = 8) {
    this.jti = Random(size)
  }
  sign (header = this.header, payload = this.payload, alg = this.alg, secret = this.secret) {
    let signature = ''
    let a = this.identifyAlg(alg)
    switch (a.use) {
      case 'HMAC':
        signature = HMAC(secret, a.base)(`${header}.${payload}`)
        break
      default:
        signature = ''
    }
    return BASE64URL.encode(signature)
  }
  encode (header = this.header, payload = this.payload, alg = this.alg, secret = this.secret) {
    var encoded = {
      header: BASE64URL(JSON.stringify(header)),
      payload: BASE64URL(JSON.stringify(payload)),
      signature: ''
    }
    this.signature = encoded.signature = this.sign(encoded.header, encoded.payload, header.alg, secret)
    this._encoded = `${encoded.header}.${encoded.payload}.${encoded.signature}`
    return this._encoded
  }
  decode (enc = this._encoded, secret = this.secret) {
    let segment = String(enc).split('.')
    if (segment.length === 3) {
      let decoded = {
        header: JSON.parse(BASE64URL.decode(segment[0])),
        payload: JSON.parse(BASE64URL.decode(segment[1])),
        signature: segment[2]
      }
      decoded.valid = (this.sign(segment[0], segment[1], decoded.header.alg, secret) === segment[2])
      return decoded
    } else {
      return {valid: false}
    }
  }
  verify (enc = this._encoded, secret = this.secret) {
    return this.decode(enc, secret).valid
  }
}
