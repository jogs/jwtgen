(function(){
  const crypto = require('crypto');
  module.exports= function(alg = 'sha256'){
    const hash = crypto.createHash(alg);
    return function(data){
      var dig = '';
      hash.on('readable', () => {
        var data = hash.read();
        if (data){
          dig = data.toString('hex');
        }
      });

      hash.write(data);
      hash.end();

      return dig;
    }
  }
})();
