(function(){
  const crypto = require('crypto');
  module.exports= function(secret = 'my awesome secret!',alg = 'sha256'){
    return function(data){
      const rsa = crypto.createSign('RSA-'+alg.toUpperCase());
      rsa.write(data);
      rsa.end();

      return rsa.sign(secret, 'hex');
    }
  }
})();
