const crypto = require('crypto')
module.exports = (secret = 'my awesome secret!', alg = 'sha256') => {
  const hmac = crypto.createHmac(alg, secret)
  return (data) => {
    var dig = ''
    hmac.on('readable', () => {
      var data = hmac.read()
      if (data) {
        dig = data.toString('ascii')
      }
    })

    hmac.write(data)
    hmac.end()

    return dig
  }
}
