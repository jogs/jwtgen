(function(){
  const crypto = require('crypto');
  module.exports= function(secret = 'my awesome secret!',alg = 'sha256'){
    return function(data){
      const ecdsa = crypto.createSign(alg);
      ecdsa.write(data);
      ecdsa.end();

      return ecdsa.sign(secret).toString('hex');
    }
  }
})();
